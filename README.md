River Runner Web Service
==============================

This web service retrieves data from the National Weather Service for the St. Francis River at Roselle, MO.

Install
--------------

Copy files to a webdirectory of your choosing.

Usage
--------------

To query the service, simply load the index.php file. The service accepts one optional parameter (format), which is used to set the output format. Two values are accepted: "xml" and "json". XML is the default output format.

To see an working of example of this web service, load one of the following URLs in your favorite browser:

- http://service.chrisamelung.com/river-runner/
- http://service.chrisamelung.com/river-runner/?format=json

The following attributes are returned:

- *id*: NWS guage id
- *name*: Name of the river and guage
- *date_time*: Date and time for the current guage reading
- *guage_level*: Latest guage reading for the river level
- *converted_level*: guage_level converted to the paddler's gauge on the hwy D bridge
- *converted_units*: Inches above 0 or feet above D bridge if level is above 45 inches
- *conditions*: Description of the current river level, ex. high, low
- *status*: Status of the river based on the guage readings for the past hour, ex. rising, falling

Acknowledgements
--------------

This service wouldn't have been possible without the free data provided by the National Weather Service.

Also, thanks to all the developers who posted code examples online. I borrowed from so many of you.
