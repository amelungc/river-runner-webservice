<?php

/**
 *  The River Runner web service
 *  @author  Chris Amelung <amelungc@gmail.com>
 *  @version  v1.0
 *  @return  River level information in XML or JSON format
 */

// the Functions file
require_once 'functions.php';

// Set the timezone
date_default_timezone_set("America/Chicago");

// Get optional parameter for the output format, xml is the default
$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml';

// Get data from http://water.weather.gov/ahps2/hydrograph_to_xml.php?gage=rozm7&output=xml
$xml = simplexml_load_file('http://water.weather.gov/ahps2/hydrograph_to_xml.php?gage=rozm7&output=xml');
if( ! $xml || $xml->getName() != 'site' ) {

	// Invalid data request, create well-formatted error result
	$results_arr = array(	'id'				=> 0,
							'name'				=> 'undefined',
							'date_time' 		=> date("Y-m-d H:i:s"),
							'gauge_level'		=> 'undefined',
							'converted_level'	=> 'undefined',
							'converted_units'	=> 'undefined',
							'conditions'		=> 'undefined',
							'status'			=> 'undefined'
						);
} else {

	// Get the river name and gauge id
	$gauge_id = (string)$xml["id"];
	$river_name = (string)$xml["name"];

	// Get current readings
	$date_time = date("Y-m-d H:i:s", strtotime($xml->observed->datum[0]->valid));
	$gauge_level = floatval($xml->observed->datum[0]->primary);

	// Determin river conditions, loosly based on http://www.americanwhitewater.org/content/River/detail/id/2921/#tab-flow
	$low_conditions = [	'Knee-high to a grasshopper.',
						'Bone dry.',
						'Non-existent.'];
	$conditions = 'extremely low';
	if( $gauge_level >= 12.00 ) {
		$conditions = 'extremely high';
	} else if( $gauge_level >= 6.00 ) {
		$conditions = 'high';
	} else if( $gauge_level >= 4.00 ) {
		$conditions = 'runnable';
	} else if( $gauge_level >= 3.00 ) {
		$conditions = 'low';
	} else {
		$conditions = $low_conditions[rand(0, count($low_conditions) - 1)];
	}

	// Convert gauge level to paddler's gaugle reading on the old hwy D bridge
	$converted_level = 0;
	$converted_units = 'inches';
	if( $gauge_level > 6 ) {
		$converted_level = ($gauge_level - 6.0) * 5/4;
		$converted_units = 'ft over bridge';
	} else {
		$converted_level = ($gauge_level - 3.0) * 15;
	}

	// Get the level from 1 hour ago and determine if the river is rising or falling
	$old_level = $xml->observed->datum[4]->primary;
	$status = 'flat';
	if( $gauge_level > $old_level ) {
		$status = 'rising';
	} else if( $gauge_level < $old_level ) {
		$status = 'falling';
	}

	// Package up results in an array
	$results_arr = array(	'id'				=> $gauge_id,
							'name'				=> $river_name,
							'date_time' 		=> $date_time,
							'gauge_level'		=> $gauge_level,
							'guage_level'		=> $gauge_level,
							'converted_level'	=> $converted_level,
							'converted_units'	=> $converted_units,
							'conditions'		=> $conditions,
							'status'			=> $status
						);
	}

// Convert results to requested format and send
if( $format == 'json' ) {
	sendResponse( 200, json_encode( $results_arr ), 'application/json' );
} else if ( $format == 'xml' ) {
	// create new instance of simplexml
	$output_xml = new SimpleXMLElement('<river/>');

	// function callback
	array2XML($output_xml, $results_arr);

	sendResponse( 200, $output_xml->asXML(), 'text/xml' );
} else {
	sendResponse( 400, 'Invalid request' );
}

?>